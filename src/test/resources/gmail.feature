Feature: Move message to important folder

Scenario Outline: Move message
Given I open gmail
When I login as user whith "<login>" and "<password>"
Then I move to important folder 3 messages and delete them

Examples:
	| login |password|
	| testng.userdrive@gmail.com	     | 1111test	|
	| selenium.test337@gmail.com	     | Qwerty333	|
	| ta.lab.testprofile@gmail.com	     | ownerofthisPROFILE	|




#Given I open gmail.com
#When I enter <login> in login input
#Then I click next button
#When I enter <password> in pass input
#Then I click next button
#When I login as user
#Then I check 3 messages
#And Click option button
#And Click move to important folder button
#When Messages were moved to important folder
#Then Delete messages
#And Check that messages were deleted

