package gmailTest.test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;
import java.util.concurrent.TimeUnit;



public class gmailMoveMessageToImpFolder {
    protected WebDriver driver;

    @Before
    public void setURL(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver=new ChromeDriver();
        driver.manage().window().maximize();

    }

    @Given("I open gmail$")
    public void I_open_gmail(){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("gmail.com");
    }

    @When("^I login as user with \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void I_login_as_user(String login, String password){
        driver.findElement(By.xpath("//*[@id='identifierId']")).sendKeys(login);
        driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("input[type='password']")).sendKeys(password);
        driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
    }

    @Then("^I move to important folder 3 messages and delete them$")
    public void I_move_to_important_folder_3_messages_and_delete_them(){
        List<WebElement> selectMessage=driver.findElements(By.xpath("//div[@class='oZ-jc T-Jo J-J5-Ji ' and @role='checkbox']"));
        for (int i = 0; i < 3; i++) {
            selectMessage.get(i).click();
        }
        driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji mA nf T-I-ax7 L3' and @role='button']//span[@class='asa bjy']")).click();
        driver.findElement(By.xpath("span[@class='ait'//div[@class='G-asx T-I-J3 J-J5-Ji']")).click();
        driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs mA' and @role='button']//div[@class='asa']")).click();
        driver.close();
    }

    @After
    public void closeBrowser(){
        driver.quit();
    }
}
