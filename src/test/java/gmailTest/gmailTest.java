package gmailTest;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@Cucumber.Options(features = "src\\test\\resources\\gmail.feature",
        glue = "test\\gmailMoveMessageToImpFolder",
        format = {"pretty"})
public class gmailTest {
}
